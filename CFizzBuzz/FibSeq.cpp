#include "stdafx.h"
#include <stdio.h>

typedef struct _FibonacciData {
	int iLowerValue;
	int iHigherValue;
} FibonacciData;

_FibonacciData * fib;

int fib_seq(_FibonacciData * fibonacciData) {

	int iNewHighest = fibonacciData->iLowerValue + fibonacciData->iHigherValue;

	if (iNewHighest > 5000) {
		printf("Over");
		return 1;
	}

	fibonacciData->iLowerValue = fibonacciData->iHigherValue;
	fibonacciData->iHigherValue = iNewHighest;
	printf("Fib: %d\n", iNewHighest);
	fib_seq(fibonacciData);
	return 1;
}
int fib_seq_start() {
	fib = new _FibonacciData;
	fib->iLowerValue = 1;
	fib->iHigherValue = 2;
	fib_seq(fib);
	delete fib;
	fib = 0;
	return 1;
}

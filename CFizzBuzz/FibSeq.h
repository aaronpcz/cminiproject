typedef struct FibonacciData FibonacciData;

int fib_seq_start();

int fib_seq(FibonacciData* data);
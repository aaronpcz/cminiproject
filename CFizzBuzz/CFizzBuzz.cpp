// CFizzBuzz.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include "FuzzBuzz.h"
#include "FibSeq.h"

int _tmain(int argc, _TCHAR* argv[]) {
	fizz_buzz_loop_given_size(20);
	fib_seq_start();
	_getch();
	return 0;
}


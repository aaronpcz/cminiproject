
#include "stdafx.h"
#include <stdio.h>

int run_fizz_buzz_loop_x_times(int iMaxLoopNumber) {
	int iStart;
	for (iStart = 1; iStart <= iMaxLoopNumber; iStart++) {
		if (!(iStart % 5) && !(iStart % 3)) {
			printf("%d Fizz Buzz\n", iStart);
		}
		else if (!(iStart % 5)) {
			printf("%d Fizz\n", iStart);
		}
		else if (!(iStart % 3)) {
			printf("%d Buzz\n", iStart);
		}
		else {
			printf("%d\n", iStart);
		}
	}

	return 1;
}


int fizz_buzz_loop() {
	run_fizz_buzz_loop_x_times(20);
	return 1;
}

int fizz_buzz_loop_given_size(int size) {
	run_fizz_buzz_loop_x_times(size);
	return 1;
}



